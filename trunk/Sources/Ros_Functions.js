//#region ROS General

// This function connects to the rosbridge server running on the local computer on port 9090
var rbServer = new ROSLIB.Ros({
    url : 'ws://' + location.host + ':9090'
 });

 // This function is called upon the rosbridge connection event
 rbServer.on('connection', function() {
    document.getElementById('feedback').innerHTML = "Connected to ROS server.";
 });

// This function is called when there is an error attempting to connect to rosbridge
rbServer.on('error', function(error) {
    document.getElementById('feedback').innerHTML = "Error connecting to ROS server.";
});

// This function is called when the connection to rosbridge is closed
rbServer.on('close', function() {
    document.getElementById('feedback').innerHTML = "Connected to ROS server.";
 });
 //#endregion
 

//#region Image processing

// Publish
var HoughParams_topic = new ROSLIB.Topic({
    ros : rbServer,
    name : '/hough_params',
    messageType : 'std_msgs/Float32MultiArray'
});

var Hough_Params_msg = new ROSLIB.Message({
    data : [0,0,0,0,0,0]
});

function pubHoughParams() {    
    Hough_Params_msg.data[0] = Number(txbDp.value);
    Hough_Params_msg.data[1] = Number(txbMinDist.value);
    Hough_Params_msg.data[2] = Number(txbParam1.value);
    Hough_Params_msg.data[3] = Number(txbParam2.value);
    Hough_Params_msg.data[4] = Number(txbMinRadius.value);
    Hough_Params_msg.data[5] = Number(txbMaxRadius.value);
    HoughParams_topic.publish(Hough_Params_msg);
}

// Subscribe
/*var listener_servo3 = new ROSLIB.Topic({
    ros : rbServer,
    name : '/servo3',
    messageType : 'std_msgs/Int16'
  });

  listener_servo3.subscribe(function(message) {
    console.log('Received message on ' + listener_servo3.name + ': ' + message.data);
    txbServo3.value = message.data;
  });*/

//#endregion